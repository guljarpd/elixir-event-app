--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2
-- Dumped by pg_dump version 12.2

-- Started on 2020-09-20 21:14:34 IST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2966 (class 1262 OID 16414)
-- Name: event_app_dev; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE event_app_dev WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_IN' LC_CTYPE = 'en_IN';


ALTER DATABASE event_app_dev OWNER TO postgres;

\connect event_app_dev

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 208 (class 1259 OID 16508)
-- Name: attendees; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.attendees (
    id bigint NOT NULL,
    user_id integer,
    event_id integer,
    request character varying(255),
    is_cancelled boolean DEFAULT false NOT NULL,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.attendees OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 16506)
-- Name: attendees_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.attendees_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.attendees_id_seq OWNER TO postgres;

--
-- TOC entry 2967 (class 0 OID 0)
-- Dependencies: 207
-- Name: attendees_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.attendees_id_seq OWNED BY public.attendees.id;


--
-- TOC entry 206 (class 1259 OID 16497)
-- Name: events; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.events (
    id bigint NOT NULL,
    description text,
    type character varying(255),
    duration integer,
    host character varying(255),
    location character varying(255),
    user_id integer,
    booking_status character varying(255),
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    event_date timestamp without time zone
);


ALTER TABLE public.events OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16495)
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.events_id_seq OWNER TO postgres;

--
-- TOC entry 2968 (class 0 OID 0)
-- Dependencies: 205
-- Name: events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.events_id_seq OWNED BY public.events.id;


--
-- TOC entry 202 (class 1259 OID 16415)
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.schema_migrations (
    version bigint NOT NULL,
    inserted_at timestamp(0) without time zone
);


ALTER TABLE public.schema_migrations OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16473)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255),
    type character varying(255),
    username character varying(255),
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16471)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- TOC entry 2969 (class 0 OID 0)
-- Dependencies: 203
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 2817 (class 2604 OID 16511)
-- Name: attendees id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.attendees ALTER COLUMN id SET DEFAULT nextval('public.attendees_id_seq'::regclass);


--
-- TOC entry 2816 (class 2604 OID 16500)
-- Name: events id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.events ALTER COLUMN id SET DEFAULT nextval('public.events_id_seq'::regclass);


--
-- TOC entry 2815 (class 2604 OID 16476)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 2960 (class 0 OID 16508)
-- Dependencies: 208
-- Data for Name: attendees; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.attendees VALUES (4, 5, 3, 'PENDING', false, '2020-09-20 14:30:26', '2020-09-20 14:30:26');
INSERT INTO public.attendees VALUES (1, 1, 3, 'CONFIRMED', false, '2020-09-20 13:52:51', '2020-09-20 15:18:00');
INSERT INTO public.attendees VALUES (3, 1, 3, 'CONFIRMED', true, '2020-09-20 14:27:43', '2020-09-20 15:26:00');


--
-- TOC entry 2958 (class 0 OID 16497)
-- Dependencies: 206
-- Data for Name: events; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.events VALUES (3, 'ndustry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled ', 'OFFICE_PARTY', 120, 'Ramesh', 'Bangaluru', 1, 'APPROVED', '2020-09-20 12:04:02', '2020-09-20 12:04:02', '2020-09-20 18:30:00');
INSERT INTO public.events VALUES (4, 'ndustry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled ', 'BIRTHDAY_PARTY', 150, 'Raju', 'Bangaluru', 1, 'APPROVED', '2020-09-20 12:06:37', '2020-09-20 12:06:37', '2020-09-21 18:30:00');


--
-- TOC entry 2954 (class 0 OID 16415)
-- Dependencies: 202
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.schema_migrations VALUES (20200913120418, '2020-09-13 12:04:31');
INSERT INTO public.schema_migrations VALUES (20200913120952, '2020-09-13 12:09:57');
INSERT INTO public.schema_migrations VALUES (20200913132426, '2020-09-13 13:25:23');
INSERT INTO public.schema_migrations VALUES (20200915152115, '2020-09-15 15:21:31');


--
-- TOC entry 2956 (class 0 OID 16473)
-- Dependencies: 204
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.users VALUES (1, 'Guljar', 'USER', 'guljar', '2020-09-13 13:56:20', '2020-09-13 13:56:20');
INSERT INTO public.users VALUES (2, 'Guljar Pd', 'ADMIN', 'guljar_pd', '2020-09-13 13:57:42', '2020-09-13 13:57:42');
INSERT INTO public.users VALUES (5, 'John', 'USER', 'john', '2020-09-13 13:59:15', '2020-09-13 13:59:15');
INSERT INTO public.users VALUES (6, 'John Ad', 'ADMIN', 'john_ad', '2020-09-13 14:15:35', '2020-09-13 14:15:35');


--
-- TOC entry 2970 (class 0 OID 0)
-- Dependencies: 207
-- Name: attendees_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.attendees_id_seq', 4, true);


--
-- TOC entry 2971 (class 0 OID 0)
-- Dependencies: 205
-- Name: events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.events_id_seq', 4, true);


--
-- TOC entry 2972 (class 0 OID 0)
-- Dependencies: 203
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 10, true);


--
-- TOC entry 2827 (class 2606 OID 16514)
-- Name: attendees attendees_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.attendees
    ADD CONSTRAINT attendees_pkey PRIMARY KEY (id);


--
-- TOC entry 2825 (class 2606 OID 16505)
-- Name: events events_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- TOC entry 2820 (class 2606 OID 16419)
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- TOC entry 2822 (class 2606 OID 16481)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2823 (class 1259 OID 16482)
-- Name: users_username_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX users_username_index ON public.users USING btree (username);


-- Completed on 2020-09-20 21:14:34 IST

--
-- PostgreSQL database dump complete
--

