defmodule EventApp.EventsTest do
  use EventApp.DataCase

  alias EventApp.Events

  describe "events" do
    alias EventApp.Events.Event

    @valid_attrs %{booking_status: "some booking_status", description: "some description", duration: 42, host: "some host", location: "some location", type: "some type", user_id: 42}
    @update_attrs %{booking_status: "some updated booking_status", description: "some updated description", duration: 43, host: "some updated host", location: "some updated location", type: "some updated type", user_id: 43}
    @invalid_attrs %{booking_status: nil, description: nil, duration: nil, host: nil, location: nil, type: nil, user_id: nil}

    def event_fixture(attrs \\ %{}) do
      {:ok, event} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Events.create_event()

      event
    end

    test "list_events/0 returns all events" do
      event = event_fixture()
      assert Events.list_events() == [event]
    end

    test "get_event!/1 returns the event with given id" do
      event = event_fixture()
      assert Events.get_event!(event.id) == event
    end

    test "create_event/1 with valid data creates a event" do
      assert {:ok, %Event{} = event} = Events.create_event(@valid_attrs)
      assert event.booking_status == "some booking_status"
      assert event.description == "some description"
      assert event.duration == 42
      assert event.host == "some host"
      assert event.location == "some location"
      assert event.type == "some type"
      assert event.user_id == 42
    end

    test "create_event/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Events.create_event(@invalid_attrs)
    end

    test "update_event/2 with valid data updates the event" do
      event = event_fixture()
      assert {:ok, %Event{} = event} = Events.update_event(event, @update_attrs)
      assert event.booking_status == "some updated booking_status"
      assert event.description == "some updated description"
      assert event.duration == 43
      assert event.host == "some updated host"
      assert event.location == "some updated location"
      assert event.type == "some updated type"
      assert event.user_id == 43
    end

    test "update_event/2 with invalid data returns error changeset" do
      event = event_fixture()
      assert {:error, %Ecto.Changeset{}} = Events.update_event(event, @invalid_attrs)
      assert event == Events.get_event!(event.id)
    end

    test "delete_event/1 deletes the event" do
      event = event_fixture()
      assert {:ok, %Event{}} = Events.delete_event(event)
      assert_raise Ecto.NoResultsError, fn -> Events.get_event!(event.id) end
    end

    test "change_event/1 returns a event changeset" do
      event = event_fixture()
      assert %Ecto.Changeset{} = Events.change_event(event)
    end
  end
end
