defmodule EventAppWeb.UserController do
  use EventAppWeb, :controller

  alias EventApp.Account
  alias EventApp.Account.User

  #
  alias EventApp.Helper

  action_fallback EventAppWeb.FallbackController

  def index(conn, _params) do

    case Helper.validate_path(conn.path_info, "ADMIN") do
      true ->
        users = Account.list_users()
        render(conn, "index.json", users: users)
      false ->
        conn
        |> put_status(403)
        |> render("error.json", error: %{message: "Unauthorized access"})
    end
  end

  def create(conn, %{"user" => user_params}) do
    # IO.inspect(conn.path_info)
    # validate user and admin access with API scope
    case Helper.validate_path(conn.path_info, user_params["type"]) do
      true ->
        with {:ok, %User{} = user} <- Account.create_user(user_params) do
          conn
          |> put_status(:created)
          |> put_resp_header("location", Routes.user_path(conn, :show, user))
          |> render("show.json", user: user)
        end
      false ->
        conn
        |> put_status(403)
        |> render("error.json", error: %{message: "Unauthorized access"})
    end
  end

  def show(conn, %{"id" => id}) do
    user = Account.get_user!(id)
    render(conn, "show.json", user: user)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Account.get_user!(id)

    with {:ok, %User{} = user} <- Account.update_user(user, user_params) do
      render(conn, "show.json", user: user)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Account.get_user!(id)

    with {:ok, %User{}} <- Account.delete_user(user) do
      send_resp(conn, :no_content, "")
    end
  end
end
