defmodule EventAppWeb.EventController do
  use EventAppWeb, :controller

  alias EventApp.Events
  alias EventApp.Events.Event

  #
  alias EventApp.Helper

  action_fallback EventAppWeb.FallbackController

  def index(conn, _params) do
    case Helper.validate_path(conn.path_info, "ADMIN") do
      true ->
        events = Events.list_events()
        render(conn, "index.json", events: events)
      false ->
        conn
        |> put_status(403)
        |> render("error.json", error: %{message: "Unauthorized access"})
    end
  end

  def create(conn, %{"event" => event_params}) do
    with {:ok, %Event{} = event} <- Events.create_event(event_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.event_path(conn, :show, event))
      |> render("show.json", event: event)
    end
  end
  # get by event id.
  def show(conn, %{"id" => id}) do
    event = Events.get_event!(id)
    render(conn, "show.json", event: event)
  end

  # get all list by user id.
  def get_by_user(conn, %{"user_id" => user_id}) do
    events = Events.get_event_by_user_id!(user_id)
    render(conn, "index.json", events: events)
  end

  # show user event
  def show_user_event(conn, %{"id" => id}) do
    event = Events.get_event!(id)
    render(conn, "show.json", event: event)
  end

  def update(conn, %{"id" => id, "event" => event_params}) do
    event = Events.get_event!(id)

    with {:ok, %Event{} = event} <- Events.update_event(event, event_params) do
      render(conn, "show.json", event: event)
    end
  end

  def delete(conn, %{"id" => id}) do
    event = Events.get_event!(id)

    with {:ok, %Event{}} <- Events.delete_event(event) do
      send_resp(conn, :no_content, "")
    end
  end
end
