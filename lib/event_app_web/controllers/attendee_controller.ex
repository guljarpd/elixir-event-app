defmodule EventAppWeb.AttendeeController do
  use EventAppWeb, :controller

  alias EventApp.Events.Attendees
  alias EventApp.Events.Attendee

  action_fallback EventAppWeb.FallbackController

  def index(conn, _params) do
    attendees = Attendees.list_attendees()
    render(conn, "index.json", attendees: attendees)
  end

  def create(conn, %{"attendee" => attendee_params}) do
    with {:ok, %Attendee{} = attendee} <- Attendees.create_attendee(attendee_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.attendee_path(conn, :show, attendee))
      |> render("show.json", attendee: attendee)
    end
  end

  def show(conn, %{"id" => id}) do
    attendee = Attendees.get_attendee!(id)
    render(conn, "show.json", attendee: attendee)
  end

  # get event id.
  def get_by_event(conn, %{"event_id" => event_id}) do
    attendees = Attendees.get_id_event_id!(event_id)
    render(conn, "index.json", attendees: attendees)
  end

  # get user and event id
  def get_by_user_event(conn, %{"event_id" => event_id, "user_id" => user_id}) do
    attendees = Attendees.get_by_user_event_id!(event_id, user_id)
    # IO.inspect(attendee)
    render(conn, "index.json", attendees: attendees)
  end

  # get by user id
  def get_by_user(conn, %{"user_id" => user_id}) do
    attendees = Attendees.get_by_user_id!(user_id)
    render(conn, "index.json", attendees: attendees)
  end

  #
  def confirm_count_user(conn, %{"user_id" => user_id}) do
    count = Attendees.get_confirm_count_user_id!(user_id)
    IO.inspect(count)
    render(conn, "count.json", count: count)
  end

  #
  def cancel_count_user(conn, %{"user_id" => user_id}) do
    count = Attendees.get_cancel_count_user_id!(user_id)
    render(conn, "count.json", count: count)
  end

  #
  def confirm_count(conn, _params) do
    count = Attendees.get_confirm_count!()
    render(conn, "count.json", count: count)
  end

  #
  def cancel_count(conn, _params) do
    count = Attendees.get_cancel_count!()
    render(conn, "count.json", count: count)
  end

  #
  def update(conn, %{"id" => id, "rsvp" => attendee_params}) do
    attendee = Attendees.get_attendee!(id)

    with {:ok, %Attendee{} = attendee} <- Attendees.update_attendee(attendee, attendee_params) do
      render(conn, "show.json", attendee: attendee)
    end
  end

  def delete(conn, %{"id" => id}) do
    attendee = Attendees.get_attendee!(id)

    with {:ok, %Attendee{}} <- Attendees.delete_attendee(attendee) do
      send_resp(conn, :no_content, "")
    end
  end
end
