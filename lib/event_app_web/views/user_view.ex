defmodule EventAppWeb.UserView do
  use EventAppWeb, :view
  alias EventAppWeb.UserView

  def render("index.json", %{users: users}) do
    %{data: render_many(users, UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{id: user.id,
      name: user.name,
      type: user.type,
      username: user.username}
  end
  # custom error view
  def render("error.json", %{error: error_data}) do
    %{error: error_data}
  end
end
