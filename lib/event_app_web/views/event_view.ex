defmodule EventAppWeb.EventView do
  use EventAppWeb, :view
  alias EventAppWeb.EventView

  def render("index.json", %{events: events}) do
    %{data: render_many(events, EventView, "event.json")}
  end

  def render("show.json", %{event: event}) do
    %{data: render_one(event, EventView, "event.json")}
  end

  def render("event.json", %{event: event}) do
    %{id: event.id,
      description: event.description,
      type: event.type,
      duration: event.duration,
      host: event.host,
      location: event.location,
      user_id: event.user_id,
      booking_status: event.booking_status,
      event_date: event.event_date
    }
  end
end
