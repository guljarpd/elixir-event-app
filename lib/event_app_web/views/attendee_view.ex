defmodule EventAppWeb.AttendeeView do
  use EventAppWeb, :view
  alias EventAppWeb.AttendeeView

  def render("index.json", %{attendees: attendees}) do
    %{data: render_many(attendees, AttendeeView, "attendee.json")}
  end

  def render("show.json", %{attendee: attendee}) do
    %{data: render_one(attendee, AttendeeView, "attendee.json")}
  end

  def render("attendee.json", %{attendee: attendee}) do
    %{id: attendee.id,
      event_id: attendee.event_id,
      user_id: attendee.user_id,
      request: attendee.request,
      is_cancelled: attendee.is_cancelled
    }
  end

  #
  def render("count.json", %{count: count}) do
    %{data: %{
        count: count
      }
    }
  end
end
