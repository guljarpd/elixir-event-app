defmodule EventAppWeb.Router do
  use EventAppWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  # Scope For Users
  scope "/api/v1", EventAppWeb do
    pipe_through :api
    # Users
    # create user
    post "/users/create", UserController, :create
    # get user by id
    get "/users/:id", UserController, :show

    # Events
    # create events
    post "/events/create", EventController, :create
    # get all events by user id
    get "/events/list/:user_id", EventController, :get_by_user
    # get event by id
    get "/events/:id", EventController, :show
    # get event by user id
    get "/events/user/:id", EventController, :show_user_event

    # Attentees
    # add attendees
    post "/rsvp/create", AttendeeController, :create
    #
    get "/rsvp/show/:id", AttendeeController, :show
    # get all attendee by event id.
    get "/rsvp/event/:event_id/:user_id", AttendeeController, :get_by_user_event
    # confirm Attentees
    put "/rsvp/update/confirm/:id", AttendeeController, :update
    # cancel cancel RSVP
    put "/rsvp/update/cancel/:id", AttendeeController, :update
    # delete attendee
    delete "/rsvp/delete/:id", AttendeeController, :delete
    # get all RSVP by user id
    get "/rsvp/user/:user_id", AttendeeController, :get_by_user
    # get confirmed RSVP
    get "/rsvp/confirmed/count/:user_id", AttendeeController, :confirm_count_user
    # get cancel RSVP
    get "/rsvp/canceled/count/:user_id", AttendeeController, :cancel_count_user
  end

  # Scope For Admin
  scope "/api/admin", EventAppWeb do
    pipe_through :api
    # Users
    post "/users/create", UserController, :create
    get "/users", UserController, :index
    get "/users/:id", UserController, :show

    # Events
    # create events
    post "/events/create", EventController, :create
    # get all events by user id
    get "/events/list/:user_id", EventController, :get_by_user
    # get all events
    get "/events/list", EventController, :index
    # get event by id
    get "/events/:id", EventController, :show

    # Attentees
    # add attendees
    post "/rsvp/create", AttendeeController, :create
    #
    get "/rsvp/show/:id", AttendeeController, :show
    # get all
    get "/rsvp/all", AttendeeController, :index
    # get all attendee by event id.
    get "/rsvp/:event_id", AttendeeController, :get_by_event
    # confirm Attentees
    put "/rsvp/update/confirm/:id", AttendeeController, :update
    # cancel cancel RSVP
    put "/rsvp/update/cancel/:id", AttendeeController, :update
    # delete attendee
    delete "/rsvp/delete/:id", AttendeeController, :delete
    # get all confirmed RSVP
    get "/rsvp/confirmed/count", AttendeeController, :confirm_count
    # get all cancel RSVP
    get "/rsvp/canceled/count", AttendeeController, :cancel_count
  end



  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through [:fetch_session, :protect_from_forgery]
      live_dashboard "/dashboard", metrics: EventAppWeb.Telemetry
    end
  end
end
