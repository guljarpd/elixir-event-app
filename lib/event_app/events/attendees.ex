
defmodule EventApp.Events.Attendees do
  @moduledoc """
  The Attendees context.
  """

  import Ecto.Query, warn: false
  alias EventApp.Repo

  alias EventApp.Events.Attendee

  @doc """
  Returns the list of attendees.

  ## Examples

      iex> list_attendees()
      [%Attendee{}, ...]

  """
  def list_attendees do
    Repo.all(Attendee)
  end

  @doc """
  Gets a single attendee.

  Raises `Ecto.NoResultsError` if the Attendee does not exist.

  ## Examples

      iex> get_attendee!(123)
      %Attendee{}

      iex> get_attendee!(456)
      ** (Ecto.NoResultsError)

  """
  def get_attendee!(id), do: Repo.get!(Attendee, id)

  #
  def get_id_event_id!(event_id) do
     Repo.all(from a in Attendee,
       where: a.event_id == ^event_id
     )
  end

  #
  def get_by_user_event_id!(event_id, user_id) do
     Repo.all(from a in Attendee,
       where: a.event_id == ^event_id and a.user_id == ^user_id
     )
  end
  #
  def get_by_user_id!(user_id) do
    Repo.all(from a in Attendee,
      where: a.user_id == ^user_id
    )
  end

  #
  def get_confirm_count_user_id!(user_id) do
    Repo.one(from a in Attendee,
      select: count(a.id),
      where: a.user_id == ^user_id and a.is_cancelled == false and a.request == "CONFIRMED"
    )
  end
  #
  def get_cancel_count_user_id!(user_id) do
    Repo.one(from a in Attendee,
      select: count(a.id),
      where: a.user_id == ^user_id and a.is_cancelled == true
    )
  end
  #
  def get_confirm_count!() do
    Repo.one(from a in Attendee,
      select: count(a.id),
      where: a.is_cancelled == false and a.request == "CONFIRMED"
    )
  end
  #
  def get_cancel_count!() do
    Repo.one(from a in Attendee,
      select: count(a.id),
      where: a.is_cancelled == true
    )
  end


  @doc """
  Creates a attendee.

  ## Examples

      iex> create_attendee(%{field: value})
      {:ok, %Attendee{}}

      iex> create_attendee(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_attendee(attrs \\ %{}) do
    %Attendee{}
    |> Attendee.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a attendee.

  ## Examples

      iex> update_attendee(attendee, %{field: new_value})
      {:ok, %Attendee{}}

      iex> update_attendee(attendee, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_attendee(%Attendee{} = attendee, attrs) do
    attendee
    |> Attendee.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a attendee.

  ## Examples

      iex> delete_attendee(attendee)
      {:ok, %Attendee{}}

      iex> delete_attendee(attendee)
      {:error, %Ecto.Changeset{}}

  """
  def delete_attendee(%Attendee{} = attendee) do
    Repo.delete(attendee)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking attendee changes.

  ## Examples

      iex> change_attendee(attendee)
      %Ecto.Changeset{data: %Attendee{}}

  """
  def change_attendee(%Attendee{} = attendee, attrs \\ %{}) do
    Attendee.changeset(attendee, attrs)
  end
end
