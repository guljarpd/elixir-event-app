defmodule EventApp.Events.Event do
  use Ecto.Schema
  import Ecto.Changeset

  schema "events" do
    field :booking_status, :string
    field :description, :string
    field :duration, :integer
    field :host, :string
    field :location, :string
    field :type, :string
    field :user_id, :integer
    field :event_date, :naive_datetime

    timestamps()
  end

  @doc false
  def changeset(event, attrs) do
    event
    |> cast(attrs, [:description, :type, :duration, :host, :location, :user_id, :booking_status, :event_date])
    |> validate_required([:description, :type, :duration, :host, :location, :user_id, :booking_status, :event_date])
  end
end
