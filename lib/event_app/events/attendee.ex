defmodule EventApp.Events.Attendee do
  use Ecto.Schema
  import Ecto.Changeset

  schema "attendees" do
    field :event_id, :integer
    field :is_cancelled, :boolean, default: false
    field :request, :string
    field :user_id, :integer

    timestamps()
  end

  @doc false
  def changeset(events, attrs) do
    events
    |> cast(attrs, [:user_id, :event_id, :request, :is_cancelled])
    |> validate_required([:user_id, :event_id, :request])
  end
end
