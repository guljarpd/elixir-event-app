defmodule EventApp.Account.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :name, :string
    field :type, :string
    field :username, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :type, :username])
    |> validate_required([:name, :type, :username])
    |> unique_constraint(:username)
  end
end
