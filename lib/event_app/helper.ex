defmodule EventApp.Helper  do
  # get scope
  defp get_scope(path_info) do
    path_info|>Enum.at(1)
  end

  # check user to validate endpoint path
  def validate_path(path_info, type) do
    scope = get_scope(path_info)
    #
    cond do
      scope == "admin" and type == "ADMIN" -> true
      scope == "v1" and type == "USER" -> true
      true -> false # if nothing maches return false
    end
  end
end
