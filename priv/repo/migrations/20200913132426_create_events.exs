defmodule EventApp.Repo.Migrations.CreateEvents do
  use Ecto.Migration

  def change do
    create table(:events) do
      add :description, :text
      add :type, :string
      add :duration, :integer
      add :host, :string
      add :location, :string
      add :user_id, :integer
      add :booking_status, :string

      timestamps()
    end

  end
end
