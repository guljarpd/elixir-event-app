defmodule EventApp.Repo.Migrations.CreateAttendees do
  use Ecto.Migration

  def change do
    create table(:attendees) do
      add :user_id, :integer
      add :event_id, :integer
      add :request, :string
      add :is_cancelled, :boolean, default: false, null: false

      timestamps()
    end

  end
end
