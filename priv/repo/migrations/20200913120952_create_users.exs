defmodule EventApp.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :name, :string
      add :type, :string
      add :username, :string

      timestamps()
    end

    create unique_index(:users, [:username])
  end
end
